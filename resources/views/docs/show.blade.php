@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Документ: {{ $document->name }} от {{  $document->created_at->format('d/m/Y') }} </div>
                    <div class="card-body">
                        <div class="d-flex w-100 justify-content-between">
                            <h3>Маршрут документа</h3>
                            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addPoint">Добавить точку</button>


                            <!-- Modal -->
                            <div class="modal fade" id="addPoint" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Добавить точку</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="GET" action="{{ route('routes.create') }}">
                                                @csrf
                                                <div class="input-group">
                                                    <input type="hidden" value="{{ $document->id }}" name="document_id">
                                                    <input type="text" name="email" class="form-control" placeholder="email">
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit">Button</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="d-flex w-100">
                            <ul class="timeline">
                                @foreach ($document->routes as $item)

                                    <li>
                                        <div>Должность</div>
                                        <div>ФИО: {{ $item->user->name }} </div>
                                        Статус документа: <span class="text-success">{{ $item->signed ? 'Подписан' : 'На рассмотрении' }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">Статус</div>
                    <div class="card-body">
                        <div class="d-flex w-100 h5 border-bottom py-3">Подписи <span class="text-primary ml-auto">3 из 6</span></div>
                        @if($document->activeRoute)
                            @if($document->activeRoute->user_id == Auth::user()->id)
                                <form action="">
                                    @csrf
                                    <a href="">
                                        <button class="btn btn-primary btn-block mt-3">Подписать</button>
                                    </a>
                                </form>

                            @elseif ($document->signed(auth()->user()))
                                <button class="btn btn-success btn-block mt-3" disabled>Вы уже подписали</button>

                            @else
                                <button class="btn btn-secondary btn-block mt-3" disabled>Вы пока не можете подписать</button>
                            @endif
                        @else
                            <div class="my-3">У документа пока нет маршрута...</div>
                            <button class="btn btn-outline-primary btn-block" data-toggle="modal" data-target="#addPoint">Добавить точку</button>


                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
