<div class="modal fade" id="addDoc" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Добавить документ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('documents.store') }}" enctype="multipart/form-data" method="POST">
                    @csrf
                    <div>
                        <input class="form-control mb-3" type="text" name="name" placeholder="Название документа">
                        <input class="form-control-file mb-3" type="file" name="document" id="document">
                    </div>
                    <button class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>
