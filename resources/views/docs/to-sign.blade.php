@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Направленные мне</div>

                <div class="card-body">
                    <a href=""><button class="btn btn-block btn-outline-primary mb-3">Добавить документ</button></a>
                    <div class="list-group">
                        @foreach(auth()->user()->toSign as $item)
                            <a href="{{ route('documents.show', $item) }}" class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{ $item->author->name }}</h5>
                                    <small>{{  $item->created_at->format('d/m/Y') }}</small>
                                </div>
                                <p class="mb-1">{{ $item->name }}</p>
                                <small>Статус: @if($item->activeRoute->user_id == Auth::user()->id) Ждет вашей подписи @else Подписано 3 из 5 @endif</small>
                            </a>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4 mt-3 mt-lg-0">
            <div class="card">
                <div class="card-header">Актуальное</div>

                <div class="card-body">
                    <ul class="list-group">
{{--                        <a href="" class="list-group-item">--}}
{{--                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                На рассмотрении--}}
{{--                                <span class="badge badge-primary badge-pill">14</span>--}}
{{--                            </li>--}}
{{--                        </a>--}}

{{--                        <a href="" class="list-group-item">--}}
{{--                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                Подписаны мною--}}
{{--                                <span class="badge badge-primary badge-pill">{{ auth()->user()->signed->count() }}</span>--}}
{{--                            </li>--}}
{{--                        </a>--}}

{{--                        <a href="" class="list-group-item">--}}
{{--                            <li class="d-flex justify-content-between align-items-center">--}}
{{--                                Ждут вашей подписи--}}
{{--                                <span class="badge badge-primary badge-pill">{{ auth()->user()->toSign->count() }}</span>--}}
{{--                            </li>--}}
{{--                        </a>--}}


                    </ul>
                    <a href="">
                        <button class="btn btn-outline-primary btn-block mt-3">Архив документов</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
