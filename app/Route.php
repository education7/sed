<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{

    protected $fillable = [
      'user_id',
      'document_id',
    ];

    public function user () {
        return $this->belongsTo(User::class);
    }

    public function document () {
        return $this->belongsTo(Document::class);
    }
}
