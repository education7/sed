<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Document extends Model
{
    public function author () {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function routes () {
        return $this->hasMany(Route::class)->orderByDesc('id');
    }

    public function activeRoute () {
        return $this->hasOne(Route::class)->where('signed', false);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'routes');
    }

    public function scopeActive (Builder $query, $id) {
        return $query->whereHas('activeRoute', function ($q) use ($id) {
            $q->where('user_id', $id);
        });
    }

    public function signed ($user) {
        return ($this->routes->where('user_id', $user->id)->where('signed', true)->first()) ? true : false;
    }

}
