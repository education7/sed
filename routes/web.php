<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/documents/to-sign', 'DocumentController@toSign')->name('toSign');
Route::get('/documents/signed', 'DocumentController@signed')->name('signed');

Route::post('/documents/sign', 'DocumentController@sign')->name('sign');

Route::resource('documents', 'DocumentController');
Route::resource('routes', 'RouteController');
